Don't believe everything you hear.    

Come and see us.    

An open letter to the US media    

I am Catherine Chen, a Director of Board at Huawei. I'm in charge of public and government affairs.    

The US is a shining example of how to inspire passion for technological innovation and development. We too have been inspired by your history of creativity and hard work.    

I am writing to you in the hopes that we can come to understand some misunderstandings about us. We would like to draw your attention to the facts.    

We operate in more than 170 countries and regions, including countires like the UK, Germany, and France. We provide innovative and secure telecoms network equipment and smartphones to more than three billion people around the world.    

Huawei was founded over 30 years ago, and we are proud of our people's willingness to work in the world's most difficult and dangerous regions. We have put our hears and souls into connecting the unconnected and bridging the digital divide in undeserved locations around the world -- places where many other companies aren't willing to go.    

We build base staions in the harshest environments, like the Arctic Circle, the sahara, rainforests in South America, and even on Mount Everest. In the wake of disasters like the tsunami in Indonesia, the nunclear disaster in Japan, and the massive earthquake in Chile,, our employees were some of the first on the ground, working tirelessly to restore communications networks and support disaster relief.    

We work with many leading US companies on technology development, business consulting, and procurement. In addition, we support university research programs in the US, helping them make significant progress in communications technologies, which we believe will benefit the whole world.    

There are only so many people we can reach out to. On behalf of Huawei, I would like to invite members of the US media to visit our campuses and meet our employees. I hope that you can take what you see and hear back to your readers, viewers, and listeners, and share this message with them, to let them know that our doors always open. We would like the USB public to get to know us better, as we will you.    

If you would like to visit us, please send an email to corpcomm@huawei.com    

Don't believe everything your hear. Come and see us. We look forward to meeting you.    

Sincerely,    

Catherine Chen    
Corporate Senior Vice President    
Director of the Board    
Huawei Technologies Co., Ltd.   